---
layout: handbook-page-toc
title: "Yearlies"
description: ""
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Yearlies

Yearlies are the annual goals for the company. 

## Alignment to Three Year Strategy 

1. Yearlies are informed by the [three-year strategy](https://about.gitlab.com/company/strategy/). 
1. Like [Objectives and Key Results (OKRs)](https://about.gitlab.com/company/okrs/), each yearly is aligned to one of the three pillars of the [three year strategy](/company/strategy/#three-year-strategy). 
1. There should be at least one yearly for each strategic pillar from [three year strategy](/company/strategy/#three-year-strategy).

## Difference between Annual Plan and Yearlies

1. Yearlies come before the [Annual Plan](/handbook/finance/financial-planning-and-analysis/#plan). 
1. Yearlies contain our priorities for the fiscal year while the Annual Plan contains our budgets and our financials.  
1. We first determine our priorities for the upcoming year in the form of Yearlies, then we use these priorities to inform the budget in the Annual Plan process. 

## Difference between OKRs and Yearlies

1. [OKRs](https://about.gitlab.com/company/okrs/) have a duration of one quarter while Yearlies are annual goals with a duration of a year.
1. [OKRs](https://about.gitlab.com/company/okrs/) are composed of Objectives and Key Results while Yearlies have only one component, the annual goal.

## Cadence

1. The [three year strategy](/company/strategy/#three-year-strategy) is on a 3 year cadence and is inspiration for the Yearlies, which are on a 1 year [cadence](/company/cadence/#year). The three year strategy is reviewed as part of [E-Group offsite calendar](/offsite/#offsite-topic-calendar). 
1. Yearlies are updated during E-Group offsite as  once-a-year topic in the [offsite topic calendar](https://about.gitlab.com/company/offsite/#offsite-topic-calendar). 
1. From the time they are established by E-Group, Yearlies are the company's goals for the next 12 months unless a Yealy goal is achieved sooner.

## FY24 Yearlies

### 1. Customer Results 
 
1. Onboard xx customers to Dedicated. 
1. Ensure GitLab is easiest solution to buy by increasing eCommerce NetARR to $XM. 
1. Drive Security and Governance adoption through enablement by increasing NetARR from Ultimate uptiers by $X  

### 2. Mature the Platform 

1. Be the leading DevSecOps platform with the most Best In Class (BIC) solutions by increasing [feature maturity to 95%](https://about.gitlab.com/devops-tools/github-vs-gitlab/) in comparison to set of industry standards. 
1. Meet current user needs for reliable and predictable disaster recovery by improving the [Disaster Recovery mechanism](https://internal-handbook.gitlab.io/handbook/engineering/infrastructure/gitlab-com-disaster-recovery-overview/) to RTO: x and RPO: x.
1. Deliver [Pods](/direction/pods/) application changes to accommodate XM unique monthly active users on GitLab.com

### 3. Grow Careers 

1. Maintain GitLab's fast pace through [bias for action](/handbook/values/#bias-for-action), [iteration](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change) and [sense of urgency](/handbook/values/#sense-of-urgency) as evidenced by meeting exit criteria to retire at least 8 of 11 [Top Cross-Functional Initiatives](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives) active as of January 2023.
1. Identify [underperformance](/handbook/leadership/underperformance/) early to help GitLab team members excel as evidenced by [performance management success rate](/handbook/people-group/people-group-metrics/#performance-management) greater than 50%.
1. Enable GitLab to be successful by increasing team diversity as evidenced by increasing URG from X% to X%
