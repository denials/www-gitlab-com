---
layout: handbook-page-toc
title: "IT Engineering Development Direction"
description: "This handbook page provides information about the IT Engineering sub-department."
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

Our long term focus is on reducing or replacing IT 1.0 tech debt with a greenfield approach for IT 2.0 using our [Automation Philosophy](#) as guiding principles and focusing on Level 4 automations and engineering simpler solutions to reduce Level 5 automations to Level 4 complexity. 

Our team is good at foreshadowing and working on projects proactively that will offer benefits to the organization in 3-12 months to solve root cause problems instead of continuing to engineer workarounds for systemtic symptoms.

The overarching philosophy has shifted to focus on backend integrations development that doesn't require a frontend UI in the traditional MVC sense. We have found that eliminating a web UI saves us a lot of headache and allows us to focus more on that last 20% of business logic that we need to solve for.

Most of our next-gen automation is focused on API calls, CLI scripts, GitLab CI/CD jobs, and Slackbot interactions. 

## Leadership Highlights

> Most projects relate to a thematic "IT 2.0" greenfield direction that removes a lot of technical debt.

1. **Architecting** next-generation automation and integration between IT managed systems that provides data consistency, reliability, strong security, and audibility.
2. **Building** [Archie](#) to programmatically provision and deprovision users in GitLab, Google, Okta, and Slack groups that are provisioned and deprovisioned manually today as part of baseline entitlements, career mobility, and offboarding issue templates. This has significant time savings for dozens of team members, and mitigates many problems that we face with audit and compliance observations. Many other services we work with depend on a reliable directory source and this has the potential of being a valuable SSOT.
3. **Consolidating** and refactoring [Demo Systems](/handbook/customer-success/demo-systems) web applications and legacy tech debt.
4. **Developing** HackyStack v2 with improved cost management features, offboarding automation, Slack integration, and evaluating the future direction of Terraform environments that can be embedded in the GitLab product.
5. **Escalation engineering** and emergency response for IT Operations and Security teams, and providing assistance to team members with all of the systems that we build and manage.
6. **Factor** in cost, security, compatibility, maintainability and user experience when making decisions.
7. **Growing other team members’ skill sets** through mentorship to improve operational efficiency and encourage professional development.
8. **Helping** team members with all systems that we build and manage.
9. **Iterating** continuously as part of our company values. 

## Quick Links

- [List of Epics](https://gitlab.com/groups/gitlab-com/it/dev/-/epics)
- [Epics Gantt Chart (Development Only)](https://gitlab.com/groups/gitlab-com/it/dev/-/roadmap?state=all&sort=end_date_asc&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&progress=WEIGHT&show_progress=true&show_milestones=false&milestones_type=ALL)
- [Epics Gantt Chart (IT)](https://gitlab.com/groups/gitlab-com/it/-/roadmap?state=all&sort=end_date_asc&layout=MONTHS&timeframe_range_type=CURRENT_YEAR&progress=WEIGHT&show_progress=true&show_milestones=false&milestones_type=ALL)

### Initiatives at a Glance

- [dev&23 IT Automation v2](https://gitlab.com/groups/gitlab-com/it/dev/-/epics/23)
- [dev&26 IAM and RBAC v2](https://gitlab.com/groups/gitlab-com/it/dev/-/epics/26)
- [dev&25 HackyStack and Sandbox Cloud](https://gitlab.com/groups/gitlab-com/it/dev/-/epics/25)
- [dev&24 Demo and Training Systems v2](https://gitlab.com/groups/gitlab-com/it/dev/-/epics/24)
- [dev&22 Open Source Packages and SDKs](https://gitlab.com/groups/gitlab-com/it/dev/-/epics/22)

### Roadmap Timelines

Since IT Systems Engineers split their time between Engineering and Operations (development happens as time allows), we are not able to provide accurate timeline forecasting (including quarterly OKRs) and instead provide stack ranked priority. All roadmap start and end dates are estimates and are not commitments.

One engineer is the DRI for each epic and are managers of one for efficiency. This allows each of us to focus on our strengths and be DRIs for the projects that we're passionate about and go deep into the programming headspace as needed without coordination. We still collaborate when we run into tough engineering challenges and we still perform peer MR reviews on all code. 

We work on 1-3 projects at a time and the project priority increases as the business need increases at the discretion of the IT Engineering team. Any epics with a P1 are top of mind and are being worked on in the current quarter or are in the up next queue. Any epics with P4 are considered wishlist. Please keep in mind that projects are often interrupted with distractions from day-to-day Slack and issue requests. 
